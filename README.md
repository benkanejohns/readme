# README



## Ben Johns, [Cyber Security Specialist](https://www.linkedin.com/in/ben-johns-crisc-b0545612/)

To build trust and break down walls, this README file is for people to get to know me on a more personal level.

## Me

- I am a husband and a father of three children. Life is real busy…
- I live in Melbourne, Australia.
- I have lived in Melbourne for around twenty years.
- I was born in Western Australia and spent most of my childhood surfing and fishing on the beaches of Broome in Western Australia.
- My passion is the beach and ocean.
- When I am not working, my family is my main priority.
- On a personal level, if I get time I enjoy things like brewing beer and making home made wine. I’m not a big drinker, I just love the process and attention required to making it. Of course, I enjoy drinking the stuff, just from a taste and appreciation perspective. Drinking is not a social thing for me. Maybe when I was younger :)

## Something most people may not see of me

- When I was younger, before I start working in tech and cyber security, I used to be a music and music teacher. For a while my aspiration and goal was to be famous. Ha ha… those were the days. To be young again…I have some great memories of playing in bands and being creative.

## My strengths

- I am creative.
- I like to connect the dots.
- I am diligent and I am transparent.
- I don’t like to playing games "politics".

## My working style

- I have a very flexible working style.
- I try my best to adjust to the variables and shifting sands of a organisation as required.
- When it comes to critical thinking, I work best remotely, in my own focus space.
- I have no issue with interacting and collaborating on a team level when required.
- I have no issue addresses a group, public speaking or running collaborative workshop.
- I pride myself of taking the time and effort to be prepared for the situation at hand.

## What I assume about others

- People will work to the best of their abilities and be passionate about life. Just like it try to be…

## What I want from others

- To be trusted and be treated as an equal.

## Weaknesses

- On the outside I may look calm, most of the time I feel like an imposter.
- On the flip side I always try to turn this into a positive and let the imposter syndrome feeling drive me to be a better person and co-worker.

## My personality type

- Protagonist - https://www.16personalities.com/enfj-personality
- A protagonists (ENFJs) feel called to serve a greater purpose in life. Thoughtful and idealistic, these personality types strive to have a positive impact on other people and the world around them. They rarely shy away from an opportunity to do the right thing, even when doing so is far from easy.


## Career goals

- Cyber security & thought leadership.
- Chief Trust Officer

## How to support me as a person

- Some days I need to take a quick break from work to drop off or pick up my children from school.
- Other than that I am always work focus 9 -5 Monday-Friday, sometimes after my children are asleep.

## Ways of working together

- I do enjoy collaborating together on initiatives (Synchronous).
- These days, more and more I am enjoying and seeing the benefits of adopting a more async working style.
- Less back to back meetings, less email, more documentation, more low context material, and when needed let's meetup online or in person (sync up).

Thank you for making it this far, you got to the end!!
If you would like to give me some constructive feedback you can connect with me on [LinkedIn](https://www.linkedin.com/in/ben-johns-b0545612/) and start up a conversation.

[Ben Johns, Cyber Security Specialist](https://www.linkedin.com/in/ben-johns-b0545612/)

All the best!
Ben
